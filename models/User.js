const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: {
        type:String,
        require: true,
    },
    email: {
        type: String,
        require: true,
        unique: true,
    },
    password: {
        type:String,
        require: true,
    }
},
{
    timestamps:true
});

// Before saving the user to the database, convert the email address to lowercase in userSchema

UserSchema.pre('save', function(next) {
  if (this.email) {
    this.email = this.email.toLowerCase();
  }
  next();
});


module.exports = mongoose.model('User' , UserSchema);
